class EpisodesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_episode, only: [:show, :update, :destroy]

  def index
    @episodes = Episode.where(tv_show_id: params[:tv_show_id])
    render json: @episodes
  end

  def show
    render json: @episode
  end

  def create
    @episode = Episode.new(episode_params.merge(tv_show_id: params[:tv_show_id]))

    if @episode.save
      render json: @episode
    else
      render json: @episode, status: 400
    end
  end

  def update
    if @episode.update_attributes(episode_params)
      render json: @episode
    else
      render json: @episode, status: 400
    end
  end

  def destroy
    if @episode.destroy
      render json: @episode
    else
      render json: @episode, status: 400
    end
  end

  private

  def set_episode
    @episode = Episode.find(params[:id])
    unless @episode.accesible_to(current_user)
      render json: 'You dont have access to that resource'
    end
  end

  def episode_params
    params.require(:episode).permit(:title, :watched)
  end

end
