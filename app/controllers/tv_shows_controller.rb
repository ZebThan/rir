class TvShowsController < ApplicationController
  before_action :authenticate_user!

  before_action :set_tv_show, only: [:show, :update, :destroy]

  def index
    @tv_shows = TvShow.all
    render json: @tv_shows
  end

  def show
    render json: @tv_show
  end

  def create
    @tv_show = TvShow.new(tv_show_params.merge(user_id: current_user.id))
    if @tv_show.save
      render json: @tv_show
    else
      render json: @tv_show, status: 400
    end
  end

  def update
    if @tv_show.update(tv_show_params)
      render json: @tv_show
    else
      render json: @tv_show, status: 400
    end
  end

  def destroy
    if @tv_show.destroy
      render json: @tv_show
    else
      render json: @tv_show, status: 400
    end
  end

  private

  def set_tv_show
    @tv_show = TvShow.find(params[:id])
    unless @tv_show.accesible_to(current_user)
      render json: 'You dont have access to that resource'
    end
  end

  def tv_show_params
    params.require(:tv_show).permit(:title, :description)
  end

end
