class Episode < ActiveRecord::Base
  belongs_to :tv_show
  delegate :accesible_to, to: :tv_show
end
