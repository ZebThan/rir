# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

shows = Array.new(20) do |n|
  show = TvShow.create(title: "Show #{n}")
  10.times do |nn|
    show.episodes.create(title: "#{show.title} episode #{nn}")
  end
  show
end
